import groovy.transform.CompileStatic
import okhttp3.Call
import okhttp3.Callback
import okhttp3.Response
import org.jetbrains.annotations.NotNull

import java.util.concurrent.CompletableFuture

@CompileStatic
class OkHttpFuture extends CompletableFuture<Response> implements Callback {

    @Override
    void onFailure(@NotNull Call call, @NotNull IOException e) {
        this.completeExceptionally(e)
    }

    @Override
    void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
        this.complete(response)
    }

}
