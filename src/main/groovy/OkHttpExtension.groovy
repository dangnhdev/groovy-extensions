import groovy.json.JsonSlurper
import groovy.transform.CompileStatic
import okhttp3.*

import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager
import java.security.SecureRandom
import java.security.cert.X509Certificate
import java.util.concurrent.CompletableFuture

@CompileStatic
class OkHttpExtension {
    static OkHttpClient.Builder ignoreSsl(final OkHttpClient.Builder self){
        TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager(){
            @Override
            void checkClientTrusted(X509Certificate[] chain, String authType) {
            }

            @Override
            void checkServerTrusted(X509Certificate[] chain, String authType) {
            }

            @Override
            X509Certificate[] getAcceptedIssuers() {
                return new X509Certificate[]{}
            }
        }}
        SSLContext sslContext = SSLContext.getInstance("SSL")
        sslContext.init(null, trustAllCerts, new SecureRandom())

        self.sslSocketFactory(sslContext.getSocketFactory(), (X509TrustManager) trustAllCerts[0])
        self.hostnameVerifier{hostname, session -> true}
        return self
    }

    static Call newCall(final OkHttpClient self, final String url){
        return self.newCall(new Request.Builder().url(url).build())
    }

    static Map<String, Object> json(final ResponseBody self){
        return new JsonSlurper().parse(self.byteStream()) as Map<String, Object>
    }

    static List<Map<String, Object>> jsonArray(final ResponseBody self) {
        return new JsonSlurper().parse(self.byteStream()) as List<Map<String, Object>>
    }

    static Call postJson(final OkHttpClient self, final String url, final String jsonBody){
        Request request = new Request.Builder()
                .url(url)
                .post(RequestBody.create(jsonBody, MediaType.parse("application/json")))
                .build()
        return self.newCall(request)
    }
    static CompletableFuture<Response> enqueue(final Call self){
        def result = new OkHttpFuture()
        self.enqueue(result)
        return result
    }
}
